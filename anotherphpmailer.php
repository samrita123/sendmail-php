<?php
// https://www.inmotionhosting.com/support/email/send-email-from-a-page/using-phpmailer-to-send-mail-through-php
require("PHPMailer_5.2.0/class.PHPMailer.php");

$mail = new PHPMailer();

$mail->IsSMTP();                                      // set mailer to use SMTP
$mail->Host = "smtp.sendgrid.net";  // specify main and backup server
$mail->SMTPAuth = true;     // turn on SMTP authentication
$mail->Username = "sendgrid_username";  // SMTP username
$mail->Password = "sendgrid_password"; // SMTP password
$mail->Port = 587;

$mail->From = "from@example.com";
$mail->FromName = "Mailer";
$mail->AddAddress("email_address_to_whom_to_send_email", "Josh Adams"); // email address to whom to send the message
$mail->AddAddress("email_address_to_whom_to_send_email");                  // name is optional
$mail->AddReplyTo("info@example.com", "Information");

// $mail->WordWrap = 50;                                 // set word wrap to 50 characters
// $mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
// $mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
$mail->IsHTML(true);                                  // set email format to HTML

$mail->Subject = "Here is the subject";
$mail->Body    = "This is the HTML message body in bold!";
$mail->AltBody = "This is the body in plain text for non-HTML mail clients";

if(!$mail->Send())
{
   echo "Message could not be sent. 
";
   echo "Mailer Error: " . $mail->ErrorInfo;
   exit;
}

echo "Message has been sent";
?>